<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/wap/common/inc/tag.jsp"%>
<link rel="stylesheet" href="/wap/common/plugin/cropper/cropper.css">
<script src="/wap/common/plugin/cropper/cropper.js"></script>
<div class="info" id="divInfo">
	<input type="hidden" name="id" value="${row.ID }">
	<div class="item">
		<div class="label">标题</div>
		<div class="data">
			<input type="text" name="title" value="${row.TITLE }" class="required" placeholder="标题"/>
		</div>
	</div>
	<div class="item">
		<div class="label">副标题</div>
		<div class="data">
			<input type="text" name="subTitle" value="${row.SUB_TITLE }" class="required" placeholder="副标题"/>
		</div>
	</div>
	<div class="item">
		<div class="label">类别</div>
		<div class="data">
			<al:select name="sort" data="${sorts }" head="请选择类别" value="${row.SORT_ID }"></al:select>
		</div>
	</div>
	<div class="item">
		<div class="label">价格</div>
		<div class="data">
			<input type="number" name="price" value="${row.PRICE }" class="required" placeholder="价格"/>
		</div>
	</div>
	<div class="item">
		<div class="label">图片</div>
		<div class="data">
			<img class="al-upload-echo" data-name="upload" data-callback="fnUploadCallback" src="/wap/common/img/img_add.png" style="width:45px;">
		</div>
	</div>
	<div class="item">
		<div class="label">封面</div>
		<div class="data">
			<img class="al-img-cut" data-name="upload" data-callback="fnUploadCallback" src="/wap/common/img/img_add.png" style="width:45px;">
		</div>
	</div>
</div>
<div class="btn" onclick="fnSave()">保存</div>
<script>
//剪切上传

</script>
<script>
//上传图片
$(function(){
	$('.al-upload-echo').each(function() {
		var id = $(this).attr('id');
		if(!id){
			id = (Math.random()+'').replace('0.','echo_');
			$(this).attr('id', id);
		}
		var fieldName = $(this).data('name');
		if(!fieldName){
			fieldName = 'upload';
		}
		var fl = $("<input type='file' style='display:none;' name='file' class='al-upload-file' id='file_"+id+"' onchange='fnUpload(this);' data-flag='"+id+"' data-name='"+fieldName+"'>");
		var fm = $("<form id='uploadPic' action='#' enctype='multipart/form-data'></form>");
		fm.append(fl);
		$(this).after(fm);
		var hid = $("<input type='hidden' name='"+fieldName+"' class='al-upload-hid' id='hid_"+id+"'>");
		$(this).after(hid);
		var ser = $(this).data('server');
		if(ser){
			fl.data('server', ser);
		}
		var cbk = $(this).data('callback');
		if(cbk){
			fl.data('callback', cbk);
		}
		$(this).click(function(){
			fl.click();
		});
	});
});

function fnUpload(tar){
	var fieldName = $(tar).data('name');
	if(!fieldName){
		fieldName = 'upload';
	}
	var url = $(tar).data('server');
	if(!url){
		url = '/js/hm/fl/upload';
	}
	if(!url){
		al.tips('未设置上传服务器');
		return;
	}
	var callback = $(tar).data('callback');
	var fd = new FormData($(tar).parent()[0]);
	// fd.append("file", tar, "file");
	 $.ajax({
		 url:url,
		 type:'post',
		 dataType: 'json',
        async: false,  
        cache: false,  
        contentType: false,  
        processData: false,
		 data: fd,
		 success: function(json) {
			 var result = json['result'];
			 var data = json['data'];
			 var msg = json['message'];

			 var echo = $(tar).data('flag');
			 if(echo){
				 setTimeout(function(){
				 $('#'+echo).prop('src', data);
				 },5000);
				 $('#hid_'+echo).val(data);
			 }
			 
			 if(callback){
				// callback(result,data,msg);
			 }
		 }
	 });
}
function fnUploadCallback(){
	
}
function fnSave(){
	if(!al.validate('#divInfo')){
		return;
	}
	var params = al.packParam('#divInfo');
	al.ajax({
		url:'/js/hm/bk/js',
		data:$.param(params,true),
		callback:function(result, data, msg){
			if(result){
				location.href = 'l_1';
			}else{
				al.tips(msg);
			}
		}
	});
}
</script>