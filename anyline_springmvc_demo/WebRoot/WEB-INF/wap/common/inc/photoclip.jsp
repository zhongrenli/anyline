<%@ page language="java" isELIgnored="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script src="//source.anyline.org/plugin/photoclip/js/iscroll-zoom.js"></script>
<script src="//source.anyline.org/plugin/photoclip/js/hammer.js"></script>
<script src="//source.anyline.org/plugin/photoclip/js/jquery.photoClip.js"></script>
<script src="//source.anyline.org/plugin/photoclip/js/anyline.photoClip.js"></script>
<script>

//上传完成
function fnUploadCallback(result,data,msg, conf){
	if(result){
		console.log(conf);
		$('#'+conf['img']).attr('src',data);
		$('#'+conf['hid']).val(data);
		
	}else{
		al.tips(msg);
	}
	layer.closeAll();
}

//弹出上传层
function cfUploadImg64(img, hid, w, h, cbk){
	var _cbk = cbk;
	if(_cbk){
	}else{
		_cbk = fnUploadCallback;
	}
	
	al.imgclip({width:w, height:h, url:'/fl/u64', callback:_cbk, img:img, hid:hid});
}
</script>
	