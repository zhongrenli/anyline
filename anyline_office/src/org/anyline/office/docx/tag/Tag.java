package org.anyline.office.docx.tag;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFParagraph;

public class Tag {
	protected List<Object> items = new ArrayList<Object>();
	protected XWPFParagraph beginTag;
	protected XWPFParagraph endTag;
	public Tag addItem(Object item){
		items.add(item);
		return this;
	}
	public int getRows(){
		int result = 2;
		int size = items.size();
		for(int i=0; i<size; i++){
			Object item = items.get(i);
			if(item instanceof Tag){
				result += ((Tag)item).getRows();
			}else{
				result += 1;
			}
		}
		return result;
	}
	public boolean isEmpty(){
		return items.size() == 0;
	}
	public List<Object> getItems() {
		return items;
	}
	public void setItems(List<Object> items) {
		this.items = items;
	}
	public XWPFParagraph getBeginTag() {
		return beginTag;
	}
	public void setBeginTag(XWPFParagraph beginTag) {
		this.beginTag = beginTag;
	}
	public XWPFParagraph getEndTag() {
		return endTag;
	}
	public void setEndTag(XWPFParagraph endTag) {
		this.endTag = endTag;
	}
	
}
